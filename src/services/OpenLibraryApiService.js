import axios from "axios";

export default {
  async searchForTitle(title) {
    const foundData = await axios.get(
      "http://openlibrary.org/search.json?title=" + title.replaceAll(" ", "+")
    );

    return foundData.data.docs;
  },
  getCoverUrlFor(book, size = "M") {
    return `http://covers.openlibrary.org/b/id/${book.cover_i}-${size}.jpg`;
  },
};
