import Vue from "vue";
import Vuex from "vuex";

import OpenLibraryApiService from "../services/OpenLibraryApiService.js";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    query: "",
    books: [],
    currentBook: null,
  },
  mutations: {
    setQuery(state, newQuery) {
      state.query = newQuery;
    },
    setBooks(state, newBookList) {
      state.books = newBookList;
    },
    setCurrentBook(state, newBook) {
      state.currentBook = newBook;
    },
  },
  actions: {
    async setQuery(context, newQuery) {
      context.commit("setQuery", newQuery);
      const newBookList = await OpenLibraryApiService.searchForTitle(newQuery);
      context.commit("setBooks", newBookList);
    },
    setCurrentBook(context, newBook) {
      context.commit("setCurrentBook", newBook);
    },
    clearCurrentBook(context) {
      context.commit("setCurrentBook", null);
    },
  },
});
