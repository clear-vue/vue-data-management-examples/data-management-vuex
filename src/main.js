import Vue from "vue";
import LayoutComponent from "./LayoutComponent.vue";
import store from "./store";

Vue.config.productionTip = false;

new Vue({
  store,
  render: (h) => h(LayoutComponent),
}).$mount("#app");
