# Data Management: Vuex

This application illustrates the book search application as a Vuex
managed application. All data management is centralized in Vuex and all API calls are managed by an API service using Vuex.

Components use `mapState` to pull in data from the Vuex store into the component's `computed` properties. Components use `mapActions` to connect the component's `methods` to the Vuex store's `actions`. Using this, data is able to stay syncronized between the components and the central Vuex store.

The Vuex store is set up so that data is pulled from the `state` and all interaction with the store is done through `actions`. `actions` then call `mutations` to modify the `state`. No component calls `mutations` directly, but that is a convention of the application and not enforced by any mechanic in how the store is coded.

This illustrates some key details of this kind of application structure:

1. All data is managed by the Vuex store and application level data is kept in one place.
2. Components can connect their state to the state of the store and changes in the store are automatically updated in the components.
3. Data is changed via action calls to the Vuex store and the store handles updating the application's `state`.
4. The Vuex store is in charge of calling APIs in response to new information or requests via action calls.
5. Some components may still have props, especially those that are used to display data from an array. An example of this is `BookTiles` using `BookTile` to show each book from an array of books. Even though the book is being passed as a `prop` to `BookTile`, the data is still reactive in the Vuex store because the book is passed via reference.